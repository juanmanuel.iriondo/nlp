# Práctica de NLP de Juan Manuel Iriondo Ortega

Esta es mi práctica de NLP.

He seguido offline tu clase y he aprendido mucho. Tengo muchas cosas que repasar pero me ha gustado mucho el mundo este del NLP, NLG, etc.

los notebooks están en los enlaces de abajo. En los propios ficheros está explicado todo lo que voy haciendo.
Espero haberme explicado bien. Cualquier duda, por favor, me lo comentas.

Me he ayudado mucho de los NoteBook de clase y de algún código encontrado en internet.


## Enlace al directorio de la práctica
https://drive.google.com/drive/folders/1CDdqj4tlVSysStc-ABOuI92aX0K1mLV5?usp=sharing


## Enlace al notebook de Jupyter Modelado_de_topics.ipynb

https://drive.google.com/file/d/1qQe1A4P9hubof6M7f0YsBYyaVRZAIukw/view?usp=sharing


## Enlace al notebook de Jupyter Analisis_de_Sentimiento.ipynb 

(los datos no están balanceados)
https://drive.google.com/file/d/1hGrheI9p_o_L8wfpBHd2SfmxzAvvUI0g/view?usp=sharing

## Enlace al notebook de Jupyter Analisis_de_Sentimiento2.ipynb 

(los datos están balanceados)
https://drive.google.com/file/d/1O5RMCx2dg4Z3_r-WsdB2EVk9THJG19cE/view?usp=sharing



## Enlace al notebook de Jupyter Generacion_Lenguaje_Natural.ipynb 

(Estudio de un modelo muy sencillo sin preprocesamiento de datos)
https://drive.google.com/file/d/1r6jTDVVpVwH-9N9u4uzHO_bd8FHgBUPF/view?usp=sharing

## Enlace al notebook de Jupyter Generacion_Lenguaje_Natural2.ipynb 

(estudio de otros 2 modelos con preprocesamiento de datos)
https://drive.google.com/file/d/1sAgJ2d9BKzU4t8V325MK38AwQn7XrnBa/view?usp=sharing

## Enlace al notebook de Jupyter Generacion_Lenguaje_Natural_Resultados.ipynb 

(Una posible solución fallida)
https://drive.google.com/file/d/1_4fZa32DZNDhFHdbynnR66xultV-Bg4K/view?usp=sharing

## Enlace al notebook de Jupyter Generacion_Lenguaje_Natural_Resultados2.ipynb 

(Otra posible solución que no he conseguido que funcione)
https://drive.google.com/file/d/1SrgcfDUEki8B_xsydJuChEpa5IeDiNL0/view?usp=sharing


